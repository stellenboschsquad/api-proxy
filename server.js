// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
const request = require('request');
var cors = require('cors');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors())

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/instantaneous', function (req, res) {

    var options = {
        url: 'https://api.bridgiot.co.za/api/energy/' + req.query.deviceID + '/between/' + req.query.startTime + '/' + req.query.endTime + '/' + req.query.sampleRate,
        headers: {
            'x-access-token': ''
        }
    };

    function callback(error, response, body) {
        if (error) { return res.send(error); }
        res.send(body)
    }

    // res.send(req.query)
    request(options, callback);
});

router.get('/cumulative', function (req, res) {

    var options = {
        url: 'https://api.bridgiot.co.za/api/device/' + req.query.deviceID,
        headers: {
            'x-access-token': ''
        }
    };

    function callback(error, response, body) {
        if (error) { return res.send(error); }
        res.send(body)
    }

    // res.send(req.query)
    request(options, callback);
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);